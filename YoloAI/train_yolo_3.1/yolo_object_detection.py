import cv2
import numpy as npK
import glob
import random
import imutils


def get_position(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        print(f'x = {x}, y = {y}')


def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCL:
        cv2.circle(img = img, center=(x,y), radius=50, color=(0,255,0), thickness=-1)


def draw_line(event, x, y, color, thickness):
    listxy = []
    if event == cv2.EVENT_LBUTTONDBLCLK:
        listxy.append([x, y])
    while True:
        if event == cv2.EVENT_LBUTTONDBLCLK:
            listxy.append([x, y])
            cv2.line(img, pt1=listxy[0], pt2=listxy[1], color=color, thickness=thickness)
            listxy.pop(0)
            cv2.imshow('image', img)
            if cv2.waitKey(1) == 32:
                break


def subdivide(image, row_div, cols_div, color, thickness):
    cols = image.shape[0]
    rows = image.shape[1]
    image_mask = image.copy()
    for c in range(0, cols - cols_div, cols_div+1):
        for r in range(0, rows - row_div, row_div+1):
            cv2.rectangle(img=image_mask, pt1=(c, r), pt2=(c + cols_div, r + row_div), color=color, thickness=thickness)
    cv2.imshow("image_mask", image_mask)


# Load Yolo
net = cv2.dnn.readNet("yolov3_training_last.weights", "yolov3_testing.cfg")

# Name custom object
classes = ["salat"]

# Images path
images_path = glob.glob(r"C:\Users\BardKrzysztof\Desktop\Salata\Bounding Box\Bounding Box 5\*.png")

# print('Wysiwtlać bboxy? N0/YES')
# Bbox_statement = input()

Bbox_statement = 'NO'

# print('Rysujesz po obrazku? N0/YES')
# Draw_statement = input()

Draw_statement = 'NO'

layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

f=0

# Insert here the path of your images
random.shuffle(images_path)
# loop through all the images
for img_path in images_path:
    # Loading image
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=2, fy=2)
    height, width, channels = img.shape

    # Rotating image
    img = imutils.rotate(image=img, angle=18)

    # Detecting objects
    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.3:
                # Object detected
                # print(class_id)
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    print(f'Indeks {indexes}, pewność {confidences}')
    print('Number of objects found %d' % len(indexes))
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[class_ids[i]]
            cv2.circle(img, (x + w // 2, y + h // 2), 8, color=(255, 255, 255), thickness=-1)
            if Bbox_statement == 'YES':
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
            # cv2.putText(img, label, (x, y + 10), font, 3, color, 2)
            key = cv2.waitKey(10)
            cv2.imshow("Image", img)
    f += 1
    cv2.imwrite(r'C:\Users\BardKrzysztof\Desktop\Salata\Tamplate_match\img_save\*.png', img,)

    if Draw_statement == 'NO':

        # Image from BGR to Gray
        img_graY = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)
        # cv2.imshow("Gray Image", img_graY)

        # Setting threshold value
        thresh_val = 10
        thresh = cv2.threshold(src=img_graY, thresh=thresh_val, maxval=255, type=cv2.THRESH_BINARY)[1]
        # cv2.imshow(f'threshold value : {thresh_val}', thresh)

        # Reversing image values
        thresh_inv = cv2.bitwise_not(src=img_graY)
        # cv2.imshow(f'threshold value : {thresh_val}', thresh_inv)

        # Finding contours to 'contours' array
        contours = cv2.findContours(image=thresh_inv, mode=cv2.RETR_LIST, method=cv2.CHAIN_APPROX_SIMPLE)[0]
        print(f'[INFO] Liczba wykrytych konturów {len(contours)}')

        # Drawing example contour on original image
        # cont = 0
        # img_cnt = cv2.drawContours(image=img.copy(), contours=[contours[cont]],
        #                            contourIdx=-1, color=(0, 0, 255), thickness=2)
        # cv2.imshow('img_cnt', img_cnt)

        listx = []  # List of X coords
        listy = []  # List of Y coords

        for idx, contour in enumerate(contours):
            area = cv2.contourArea(contour=contour, oriented=True)
            # print(f'[INFO] kontur nr {idx}, ma powierzchnię {area}')
            if area == 220.0:
                M = cv2.moments(contour)
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                listx.append(cX)
                listy.append(cY)

        listxy = list(zip(listx, listy))  # zipping to lists to one
        listxy = np.array(listxy)  # creating from it Numpy type array

        for i in range(0, len(listxy)):
            x1 = listxy[i, 0]
            y1 = listxy[i, 1]
            distance = 0
            secondx = []
            secondy = []
            dist_listappend = []
            sort = []
            for j in range(0, len(listxy)):
                if i == j:
                    pass
                else:
                    x2 = listxy[j, 0]
                    y2 = listxy[j, 1]
                    distance = np.sqrt((x1-x2)**2 + (y1-y2)**2)
                    secondx.append(x2)
                    secondy.append(y2)
                    dist_listappend.append(distance)
            secondxy = list(zip(dist_listappend, secondx, secondy))
            # print('Second X, Y', secondxy)
            sort = sorted(secondxy, key=lambda second: second[0])
            sort = np.array(sort)
            # print('Sort', sort)

            from sklearn.linear_model import LinearRegression

            cv2.line(img=img, pt1=(x1, y1), pt2=(int(sort[0, 1]), int(sort[0, 2])), color=(0, 0, 255), thickness=2)
            cv2.imshow('Connected', img)
            key = cv2.waitKey(10)
    else:
        cv2.setMouseCallback('image', draw_circle)
        while True:
            cv2.imshow('image', img)
            if cv2.waitKey(1) == 27:  # ESC key
                break

    subdivide(img, 34, 34, (0, 0, 255), 1)
    #cv2.imshow('Connected', img)

    # for threshold1_val in range(0, 256, 10):
    #     for threshold2_val in range(0, 256, 10):
    #         canny = cv2.Canny(image=img_graY, threshold1=threshold1_val, threshold2=threshold2_val)
    #         cv2.imshow(f'threshold value : {threshold1_val} , {threshold2_val}', canny)
    #         cv2.waitKey(1)
    #         cv2.destroyWindow(f'threshold value : {threshold1_val}, {threshold2_val}')

    key = cv2.waitKey(0)
    cv2.destroyAllWindows()



