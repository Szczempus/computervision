import cv2
import numpy as np
import glob
import random


# Load Yolo
net = cv2.dnn.readNet("yolov3_training_last.weights", "yolov3_testing.cfg")

# Name custom object
classes = ["salat"]

# Images path
images_path = glob.glob(r"C:\Users\BardKrzysztof\Desktop\Salata\Bounding Box\Bounding Box 5\*.png")

print('Wysiwtlać bboxy? N0/YES')
Bbox_statement = input()

layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

# Insert here the path of your images
random.shuffle(images_path)
# loop through all the images
for img_path in images_path:
    # Loading image
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=2, fy=2)
    height, width, channels = img.shape

    # Detecting objects
    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.3:
                # Object detected
                #print(class_id)
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    print(f'Indeks {indexes}, pewność {confidences}')
    print('Number of objects found %d' % len(indexes))
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[class_ids[i]]
            cv2.circle(img, (x + w //2, y + h//2), 8, color=(0,0,0), thickness=-1)
            if Bbox_statement == 'YES':
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
            #cv2.putText(img, label, (x, y + 10), font, 3, color, 2)


    cv2.imshow("Image", img)

    img_graY = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)
    cv2.imshow("Gray Image", img_graY)



    # for threshold1_val in range(0, 256, 10):
    #     for threshold2_val in range(0, 256, 10):
    #         canny = cv2.Canny(image=img_graY, threshold1=threshold1_val, threshold2=threshold2_val)
    #         cv2.imshow(f'threshold value : {threshold1_val} , {threshold2_val}', canny)
    #         cv2.waitKey(1)
    #         cv2.destroyWindow(f'threshold value : {threshold1_val}, {threshold2_val}')


    key = cv2.waitKey(0)
    cv2.destroyAllWindows()



